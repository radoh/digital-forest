---
title: "2023 Predictions and Trends"
date: 2023-01-03T17:26:34+03:00
draft: false
tags:
- leagueoflegends
---

Ceci est un post pour regrouper certaines de mes prédictions (bonnes ou moins bonnes) pour l’année 2023.
Elles concernent la technologie, la politique, l’esport et bien d’autres…
Pourquoi?
Chaque année, on se dit «je le savais».
Mais l’est-on vraiment?

* esport: League of Legends
** intro
Les 3 grands serveurs que je regarde LEC, LCK et LCS, les prédictions ne seront pas par splits.
Car c’est juste pas possible de le faire.
Mais je vais donner un top 3 des équipes de la région, dans le désordre.

La vérification est de savoir qui iront au World.
** World
Commençant par la fin le World Championship.
Je pense que durant la phase de demi-finale, il n’y aura 4 équipes asiatiques.
** LEC
Pour la LEC, les trois équipes que je trouve le plus intéressantes sont:
- Heretics
- Koi
- Excel

Pourquoi pas de grands noms comme Fnatic ou G2? Car ce sont de grands classiques.

Je trouve ces trois équipes intéressantes pour leur coaching staff.
Il y a de grands noms parmi les joueurs, mais c’est la même chose pour les autres équipes.

Je pense prioritisé à regarder leurs matchs durant les saisons régulières.

Mais les trois équipes LEC qui iront au World seront: Excel, Koi et G2.

Pourquoi pas Team Heretic?
La réponse se trouve dans une question: est ce que Peter Dun a fait un miracle pour EG ou est ce que sa méthode fonctionne.
Si ce n’est pas un hasard alors les chances d’Excel pour aller au World diminue.

Pourquoi pas Fnatic?
Je ne pense pas que de grands noms dans une équipes fait qu’automatiquement qu’elle deviendra la meilleure.
Je n’ai pas une impression positive de longue durée de ses joueurs.
Ils sont très bons.
Mais elle me donne l’impression de Elements ou G2 avec Rekkles.
Le coaching staff devient alors plus important et je n’ai pas d’impression sur elle.

G2 car des joueurs comme MikyX et Caps semblent indépendants du staff et capable de carry une équipe.
** LCS
Les trois équipes qui iront aux World seront: C9, Flyquest et Team Liquid.

Team Liquid car même si c’est une équipe d’importation, ils n’auront pas vraiment la barrière de la langue.
Intéressante car c’est l’essai de CoreJJ pour élever les LCS au niveau des deux grandes régions asiatiques.

Flyquest car c’est une équipe avec PapaSmithy qui a fait des miracles avec 100T.
En plus elle a réussit à importer des très bons joueurs de la LCK (Prince et VicLa) à leur pick pas en mode vacancier.

C9 car la méthode gaming house reste efficace pour créer une bonne team.

Pourquoi pas de 100T?
Car regarder Bjergsen jouer n’est pas amusant.
Il n’a pas l’air de s’amuser lorsqu’il joue.
Doublelift jouer ne m’intéresse pas non plus.
La réunion des deux est une bonne histoire mais pour moi ça reste une bonne histoire.
Pas forcément synonyme de performance.
Surtout que leurs performances au World est souvent décevante.
S’ils y vont.
C’est pour cela que je mise plus sur Flyquest.
** LCK
Le top trois sera: T1, DWG KIA et Gen.G.

T1 car c’est la même équipe depuis un bon moment.
On sait de quoi elle est capable.
Pour changer, elle va être décevante durant quelques matchs pour être impressionnante ou juste être impressionnante.

DWG KIA et Gen.G pour les noms et la consistance et longévité des leurs joueurs.
* politique
Le président de Madagascar actuelle réussira à rester au pouvoir pour un mandat de plus.

Les malgaches, comme tous les humains, sont sensibles au biais de proximité.
La propagande va faire oublier les dernières années.

La misère a fait que pour continuer de vivre endurer est la seule voie.
* Kindle and Audible
Il semble qu’Amazon est en train de changer son focus.
Pendant des années, je le voyais comme une librairie en ligne.
La plateforme vendait des livres.
Puis c’est devenu un site marchant de commerce.
Maintenant, il semble que c’est devenu un Netflix-like (sachant qu’il a déjà Twitch), il ne manquait que plein de films et de séries.
Bien sur l’infrastructure d’internet qu’on le connait dépend beaucoup de l’entreprise.

L’impression qu’Amazon se concentre moins sur les livres va faire en sorte qu’il y a des chances que de meilleure version de la Kindle sont peu probable à la longue.
Ce n’est pas un si gros problème car il y a de plus en plus d’entreprises qui créent des e-reader.

Ce n’est pas si étrange si on y pense.
Amazon est déjà le boss final de l’ebook.
Elle n’a pas vraiment de puissants concurrents.

Audible appartient à Amazon.
C’est la plateforme de base pour écouter des livres audios.
C’est quasiment une monopolie.
La vidéo de [[https://www.youtube.com/watch?v=gld-uHAIRlE&t=0s][Daniel Greene]] explique la situation et la bataille de Sanderson.

Je ne suis pas (encore?) adepte des audiobooks.
Je suis passé à l’ebook pour le côté prix et practicité même si j’apprécie le format livre de poche.

Mais quand il y a besoin il y aura la création d’une solution.
[[https://www.forbes.com/sites/johnkoetsier/2023/01/05/ai-audiobooks-from-apple-digital-narration-technology-to-deliver-audio-for-millions-of-books/][AI Audiobooks From Apple]] semble  être une solution.
Bien sur ce n’est pas si magique que ça. Je suis quasiment certain qu’Apple va générer l’audiobook chez eux et vendre l’accès aux fichiers.
Cela va potentiellement aider les petits auteurs.

Si j’ai déjà acheter un ebook.
Pourquoi ne pas avoir la possibilité de moi même générer mon audiobook.
Quelle est la vrai valeur ajouté qu’Apple apporte?
Générer un programme pour créer un fichier audio et c’est quasiment tout.
Oui la qualité sera certainement supérieure à celui des autres AI mais jusqu’à quand.

J’espère qu’il y aura un projet qui pourra générer un audiobook à partir de l’ebook, localement sur son propre PC et qu’il sera open source et libre.

Dans la même ligne de penser, après presque 5 ans à utiliser une Kindle pour lire mes livres, si j’achète une liseuse ce ne sera pas une Kindle.
Je pense que beaucoup de personnes peuvent être amener à penser de même.
