---
title: "Qui en est le bénéficitaire?"
date: 2023-12-27T17:22:45
draft: false
---

Quelqu’un se présente aux élections, change le prix du paquet de biscuits, modifie son modèle économique.
Met à jour un logiciel, fait de la publicité.

La question qu’on doit se poser est: à qui bénéficie cette action?
Ce changement.

C’est dans le même style qu’il faut suivre l’argent.
Où va l’argent.

Dans les séries policières et de détectives, c’est se demander quel est le mobile (raison du crime).

Dans l’industrie pour arriver à la réponse on peut utiliser les 5 pourquoi.
En compabilité c’est voir où va l’argent.

* politique
Une personne décide de devenir candidat à la présidence.
Il fait donc une propagande.
Comment déterminer s’il est digne de mon vote?
Dans mon cas, je me suis posé la question: pourquoi est ce que ce candidat s’est présenté?
Répondre n’est pas souvent facile.
La réponse peut ne pas directement sauter aux yeux.

Une série de question peut aider à se rapprocher de la vérité.

D’où vient l’argent pour la campagne?
Fonds privés, associative, parti politique…

Pourquoi cette question de provenance de l’argent est importante?
Personne ne donne de l’argent gratuitement.
Chacun cherche un retour sur investissement.
Beaucoup veulent obtenir du pouvoir.

A qui ce candidat doit-il rendre des comptes?
C’est un moyen de voir ses influences et la légitimité de ses actions.

Dans beaucoup de pays, connaitre la raison de la candidature permet d’en éliminer beaucoup.
Et cela peut se faire avant même de regarder le programme.

Parfois, on se présente car on a déjà eut gout au pouvoir et gloire.
On veut y rester.
On n’a même pas de programme ou de désir d’améliorer la situation dans le pays.

* logiciels, système d’exploitation
Il est important de garder ses logiciels à jour.
Du moins c’est ce qu’on entend souvent dire.

La raison principale est la sécurité.
Un logiciel n’est jamais parfait.
Il faut les mettre à jour pour pouvoir régler les problèmes de sécurité.

L’autre raison est les bugs.
Il est énervant de perdre une partie de son travail à cause d’un bug.
C’est une bonne raison pour se mettre à jour.

Puis, il y a d’autres types de mise à jour.
Pourquoi est en passé de Windows 95 à XP, Vista, 7, 8, 10, 11?
Qui en a vraiment bénéficier?
Est ce que c’est l’utilisateur finale?
Je ne pense pas vraiment.

Les ordinateurs sont devenus plus puissants.
On a même changer d’architecture pour avoir plus de puissance et vitesse.
Passage de 16 à 32 à 64 bits.
De ce point de vu, l’utilisateur final en profite vraiment.
On est capable de faire plus d’actions et réalisé plus de choses grâce à un ordinateur.

Le passage de XP à Vista, d’après mes souvenir n’a pas vraiment apporter grand chose.
C’est plus jolie et personnalisable, mais fondamentalement ça n’a pas apporter grand chose.

Windows 8 a selon moi était dans le but d’intégrer les fonctionnalités tactiles.

11 est dans le but de forcer les gens à acheter de nouveaux ordinateurs.

Si on y pense, les changements de versions des systèmes d’exploitation par Microsoft ne bénéficie vraiment que Microsoft.
Le reste du monde doit juste s’y plier.

C’est un peu le cas avec Android et iOS.
Oui c’est plus rapide et tout le reste.
Mais ça bénéfice principalement les constructeurs et vendeur d’appareil.
Pour chaque changement de version majeur, il faut un matériel plus puissant.

* réseaux sociaux
On est maintenant capable de communiquer instantanément avec ses amis et famille à travers le monde.

Mais on ne s'est jamais senti aussi seul.
Avec la possibilité de discuter avec tout le monde, on le fait moins.
L'importance de chaque échange a diminuer avec la quantité.

Si on y pense, on en bénéficie quand même.
Mais le groupe qui en bénéficie le plus est l'entreprise plus que le consommateurs.
Il est un consommateur et non un acteur de sa vie sur les réseaux sociaux.

La fait d'avoir plus de données ne semble pas pour autant aider les entreprises pour les pubs.
Il est plus facile de cibler ses pubs.
Mais quand une est capable, les autres aussi, rendant l'environnement encore plus compétitif.

La mort des forums est un des effets néfastes.
Surtout que les forums appartenaient à des individus alors que les réseaux sociaux à de grandes corporation.
Mais c'est la création d'intelligence artificielle qui a bénéficié de cet afflux et monopolisation de données.

* résolution des photos et vidéos
Il y a quelques jours j'ai regardé de vieilles photos.
Elles étaient prise avec un appareil photo numérique des années 2007.
Un appareil d'entrée de gamme de l'époque.
La qualité des photos n'est pas génial, surtout comparée à celles de nos smartphones actuels.

J'avais aussi des photos prises avec les téléphones de l'époque.
On y voit encore moins bien que sur celles de l'appareil photo.

Pour les souvenirs de vacances, je suis bien heureux de l'amélioration de la qualité des appareils.

Mais ici on a un saut technologique de plus de 10 ans.
Entre un smartphone de cette année et d'il y a trois ans, la différence est minime.
Surtout si les photos sont prises dans des conditions de lumière pas trop sombre.

D'un autre côté, il y a les télévisions.
On est passé du cathodique à écran plats, LCD et autres.
D'une petite résolution, 720p à 4k.
Regarder un film 720p et 4k n'apporte pas énormément grand chose.
C'est le même film, même histoire, juste en plus jolie.
Un bon film reste un bon film.
Un mauvais film reste mauvais.

Qui a vraiment profiter de ce changement ?
Vendeur de caméra, de télévision ou l'environnement ?
