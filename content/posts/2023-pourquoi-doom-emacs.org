#+title: 2023 Pourquoi Doom Emacs
---
title: "Pourquoi j'utilise Doom Emacs"
date: 2023-12-18T12:06:47
draft : false
---
Depuis quelques semaines, sur ordinateur et l'un de mes téléphones, j'utilise Doom Emacs.
Selon Derek de la chaine YouTube DistroTube, Doom Emacs est une distribution d'Emacs (si emacs était Linux).
Doom est un framework.
Il permet d'avoir une configuration par défaut beaucoup plus agréable (subjectivement) par défaut.

Je ne suis pas un gros programmeur.
Je connais un minimun d'Elisp, C, Rust, Bash et Python pour m'aider à accomplir des choses.
Emacs est généralement utilisée pour la réflection.
Mon utilisation est l'écriture de proses plus que de programmer occasionnelement.
* lnterface agréable
De base, l'interface d'Emacs est correcte.
Il suffit d'avoir l'habitude.

On peut utiliser Emacs sans vraiment faire de changement.

Il y a des thèmes préinstallés si on aime pas celui de base.

L'avantage de Doom est que c'est beaucoup plus rafiné.
On a des icones, ligatures, émojis de base.

Grâce aux fringes, on voit les changements non commit de nos fichiers.

Il n'y a pas à besoin de les configurer.

Ce n'est pas vraiment vitale, mais ça reste agréable.

En terme d'interface, il y a aussi les raccourcis claviers.
Doom semble mélanger des raccorcis d'Emacs et Vim.
En utilisant la touche espace comme leader key.
C'est franchement agréable.

Lorsqu'on est en mode Insert, je peux utiliser sans souci les raccourcis natifs Emacs.
Ce qui fait que ça ne change pas mes habitudes.
Je ne les connais pas tous, mais le peu que je connais fait largement l'affaire.
C'est à dire les déplacement de base.
* préconfiguration
Des packages sont préconfigurés.

On peut utiliser org-roam, correction orthographique, which-key ou un truc du genre de base.
Du peu que j'ai testé, c'est bien fait.

Yasnippet est configuré avec des templates déjà fonctionnels.

Pour certains packages, il me suffit d'ajouter le dossier cible.
* rapide
C'est un peu triste à dire, mais Doom est plus rapide que ma configuration à partir de Vanilla Emacs.

J'utilise les même packages dans les deux configurations.

La différence est que sur Termux Android, à partir de Vanilla, org roam est directement accessible.
Sur Doom, après avoir lancer Emacs, lancer org-roam demande quelques secondes de plus.

La différence entre les deux n'est pas significative.
Pour l'un, le temps de lancement est plus lent.
Pour l'autre, il faut patienter après avoir lancer Emacs.
* conclusion
Après avoir utiliser et maintenant ma propre configuration pendant 5 ans, je suis passé à Doom Emacs.
C'est performant.

Il y a certainement des moyens d'améliorer le tout.
Globalement ça fonctionne comme il le faut.
Doom n'est pas un obstacle au travail ou la réflection.

Le problème est que j'ai moins d'excuses (pour ne pas dire aucun) pour procrastiner à améliorer ma configuration.
Doom s'est occupé à peu près de tout.
