---
title: "2023 retrospective lecture de la Bible"
date: 2023-12-28T18:39:17
draft: false
---
Lire la Bible est important.
Lire du début à la fin est un grand challenge.
C’est une des résolution ou objectif que beaucoup se donne chaque année.
Je me suis donné ce challenge en début d’année.

Lire la Bible en entier n’est pas facile.

Le but est de faire de la lecture de la Bible une habitude quotidienne.
Mais ce n’est pas lire dans le but de lire.
Il faut pouvoir en tirer quelquechose.
On veut approfondir sa foi et sa connaissance de Dieu.
Pouvoir appliquer la sagesse du livre dans sa vie.


* Professor Horner’s reading system
https://www.biblestudytools.com/bible-reading-plan/prof-horners-reading-system.html
J’ai commencé ce plan durant le mois de décembre 2022.

Le principe est de lire 10 chapitres par jour.

J’ai commencé à lire sur une Bible en papier.
C’est je pense une erreur.
Je suis lent pour naviguer dans la Bible.
La navigation me demande de l’énergie.
Ce n’est pas une si mauvaise chose, car après quelques jours, je suis devenu meilleur.

Le but est de lire.
Donc dépenser de l’énergie à se repérer n’est pas génial génial.

Je pense avoir persister à utiliser ce plan pendant un mois et demi.
Puis j’ai abandonné.
Non pas parce qu’il n’est pas bon.
Mais parce que je suis passé à une Bible numérique.
Une application qui m’aide à suivre la progression: l’application de Bible de YouVersion  https://www.youversion.com/the-bible-app/.
* 5×5×5 New Testament Bible Reading Plan by Navigators
https://www.navigators.org/resource/bible-reading-plans/
Le but de ce plan est de lire le Nouveau Testament en un an.

Le plan nécessite 5 minutes par jour, 5 jours par semaine.
Si on rate un jour, parce que la vie, on a deux jours par semaine pour rattraper son retard.

On peut aussi utiliser ces deux jours pour approfondir un théme, relire un passage ou lire un autre plan.

Ça ne prend pas beaucoup de temps.
Du coup même si on a pas vraiment l’envie, il suffit de commencer et ça va vite.
* the One Year Bible by Tyndale Bibles
On lit tous les jours.
Et on lit la totalité de la Bible en un an.
Mais ce n’est pas une lecture de Genése à Apocalypse.

Tous les jours, on lit une portion de l’Ancien et Nouveau Testament, Psaumes et Proverbes.

Ce plan permet de pouvoir suivre l’histoire raconté de la Bible assez facilement.
Ça demande un peu plus de temps tous les jours.
Il n’y a pas de jours de vacances.
Mais ça permet de pouvoir mieux comprendre l’histoire et acquérir l’habitude de lire.
* Plans thématiques
Durant l’année, je lis des plans thématiques.
A Paques, je lis des plan en rapport à Paques.
De même pour Noel.

L’application Bible de YouVersion propose les plans.
Je choisis ce que je trouve intéressant sur le moment.
* Plusieurs plans de lecture
L’idée vient de Chris J. Wilson.
https://www.youtube.com/watch?v=SWq-J0m5egY
Dans sa vidéo il explique pourquoi il allait faire 12 challenges de lecture de la Bible.
L’un d’eux est de lire la totalité de la Bible en 7 jours  https://www.youtube.com/watch?v=keASX8QdRtA

Le but est de lire.

Si on échoue un challenge, l’année n’est pas perdu.
Il suffit de commencer un autre plan.

Mes plans sont assez décalés.
C’est peut être un hasard, mais c’est une bonne chose.
Ça permet de ne pas lire exactement la même chose le même jour.
Exemple: One Year Bible dit de lire Matthieu, alors que 5×5×5 dit de lire Jean.

Parfois, un passage ne nous parle pas au moment de la lecture.
Mais quelque mois plus tard, si on relit ce texte, on peut en tirer des choses.
Avoir un certain décallage aide dans ce cas.

* Bible numérique, audio Bible, prise de notes
Je suis passé à la Bible numérique.
Il y a beaucoup d’applications pour smartphone pour lire la Bible.

Le plus important est de pouvoir lire sans internet.
L’autre facteur est de ne pas être dérangé par de la publicité lorsqu’on veut lire.

Grâce aux Bibles numériques, je peux choisir et changer de versions facilement.

Actuellement, les versions que j’utilise sont:
- la Bible du Semeur 2015 (BDS)
- Parole de Vie 2017 (PDV2017)
- Bible en français courant (BFC)
- Nouvelle Bible Segond (NBS)
- New Internatial version (NIV)
- New Living Translation (NLT)

Les plans de lectures sont directement dans l’application de YouVersion.
Je ne perd donc pas d’énergie à chercher ma position et objectif de la journée.
Il suffit de lire.

Sur ordinateur, mes Bibles sont de simples fichiers textes (.txt), parfois formaté en org-mode (.org).
Je les lis donc dans un simple éditeur de texte.

Souvent, j’ai les yeux fatigué.
Au lieu de lire, je passe à une version audio.
C’est pratique.
Mais, il faut rester concentrer.
Il est facile d’être distrait lors qu’on écoute.
Par contre, il faut être connecté à internet pour pouvoir écouter les audios.

Vu que j’ai les mains libres, j’ai tendance à vouloir faire autre chose en même temps.
La majorité de temps, je m’efforce de ne rien faire à part écouter.
Mais ce n’est pas facile.
Je choisis donc de faire des tâches qui ne me demande pas de concentration et qui peuvent être interrompues.
Par exemple arroser le jardin, sortir les chiens…

Je prend des notes avec Emacs.

Je surligne directement dans l’application.
Il faudrait que j’ajoute ces passages dans mon répertoire sur Emacs.
Mais je n’ai pas encore penser à la méthode.
Donc pour l’instant, les passages en surbrillances sont emprisonnés dans l’application.
* Habitudes
Dans un monde idéal, je voudrais pouvoir lire tous les jours au même endroit à la même heure.
Mais ce n’est pas toujours possible.

En début d’année, je lisais le matin au levé.
Vers mars, c’etait avant d’aller dormir.
Maintenant, c’est avant le diner dans le jardin ou avant d’aller me coucher sur mon lit.

S’il pleut, il ne m’est pas possible d’aller dans le jardin.
En voyage, je suis parfois trop fatigué pour lire le soir.
C’est s’endormir durant la lecture.

En terme de fréquence et d’habitude ce n’est pas super.
On ne peut même pas considérer cela comme une routine.

Parfois, il n’arrive de rater un ou plusieurs jours.

Mais, je commence á avoir un système.
Si je n’arrive pas à lire au réveil, je lis à midi.
Sinon c’est vers l’heure du thé.
Si ce n’est pas encore possible, lire avant de diner, durant la cuisson du diner, puis après manger, avant de me coucher.
Le principe est d’avoir un programme dans l’éventualité d’une perturbation.

Ne pas être contraint d’utiliser un livre papier aide.
On a toujours son smartphone sur soi.
C’est assez discret en public.
