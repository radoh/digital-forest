---
title: "Lol Schedule"
date: 2024-09-02T16:44:28+03:00
draft: false
tags:
- leagueoflegends
---
Je ne joue plus à League of Legends.
[[{{< ref "/posts/j-ai-joue-a-lol.org" >}}][J’ai joué à LoL.]]
La politique [[{{< ref "/posts/league-of-legends-anticheat.org" >}}][anticheat]] a renforcé l’idée de ne pas jouer au jeu.

* je n’aime pas la manière dont Vanguard a été implémenté
Vanguard est le logiciel anticheat.
Il est obligatoire.
Il ne fonctionne que sur Windows.
Or j’utilise Linux au quotidien et n’a pas de machine sur d’autre système d’exploitation.

Vanguard est un rootkit.
Il est installé au niveau du noyau.
C’est à la dire la partie la plus proche et plus profonde (proche du matériel).
Potentiellement, il peut avoir accès à tout ce qui est présent sur ordinateur.

Si on n’est pas un tricheur, il n’y a pas de raison de ne pas aimer un logiciel qui va détecter les tricheurs.
Mais, il y a une limite à ne pas depasser.
Et je pense que cette limite a été franchi.

Si on n’a rien à cacher, il n’y a pas besoin de vie privée disent-ils aussi.

Quelle est cette limite?
Ne pas faire courrir un grand risque pour l’utilisateur.
Respecter l’utilisateur.

Mon analogie est assez tiré par les cheveux.
Mais je pense qu’il n’y a pas besoin de devenir des monstres pour combattre les monstres.
Un monstre est quelqu’un qui détourne, change, transforme les lois pour accomplir ses objectifs.
Il fait en sorte d’augmenter son pouvoir par tous les moyens.
C’est renier ce en quoi il se bat pour gagner la guerre.

* le jeu reste intéressant
Le jeu continue d’être intéressant.
Il y a de nouvelles stratégies.

Les équipes comprennent de mieux en mieux comment jouer.
Il y a plusieurs types de stratégies et nuances au jeu.

C’est pour cela que regarder des parties pro reste fun.

* regarder des matchs en direct
Normalement, il suffit d’aller sur YouTube ou Twitch.
Si il y a des matchs officiels, on peut les regarder sans paywalls.
Ce qui est franchement génial.

C’est tellement mieux que d’aller regarder le championnat du monde d’Ultimate (freesbee).
Il faut payer sur un site spécialisé.
Parfois dans une langue inconnue.

C’est mieux que d’aller regarder un match de foot à la télé.
Il faut avoir un abonnement.

Pour League of Legends, il y a moins de barrière à l’entrée.
Il suffit d’aller sur un service de streaming et c’est bon.
Une connexion internet sur son appareil fait l’affaire pour regarder.

* quoi regarder?
Notre temps est limité.
On ne peut pas regarder tous les matches.

Comment choisir?

Je n’ai pas encore la méthode parfaite.

Je me limite à quelques équipes que je trouve fun à regarder.
C’est T1, BRO (pour la narrative et les commentateurs), parfois G2 et Fnatic.
La majorité des playoffs de la LCK, et parfois les finales LEC et LCS.
Puis, il y a les Worlds.

Il arrive que des podcasts arrivent à hype un match.
Ce qui fait qu’il m’arrive de regarder ces matchs (en plus).

Ce n’est pas beaucoup, mais c’est déjà des centaines d’heures de visionnage par ans.

* quand regarder?
C’est là que ce trouve le vrai défi.

Il faut aller sur lolesports pour voir le programme.
Le site s’est amélioré avec le temps.

Mais c’est franchement pas naturel.

Il y a cet adage: si ce n’est pas dans son calendrier, ce ne sera pas fait.

Il m’arrive trop souvent d’avoir ce flash.
Qui dit: oh non, il y a avait ce match que je voulais regarder, trop tard.

C’est du au fait qu’il n’est pas facile d’avoir les matchs dans son calendrier.
Il faut les ajouter manuellement.

J’aime l’idée de [[https://collegeinfogeek.com/about/meet-the-author/][Thomas Frank]] du calendrier optionnel.
Dans ce calendrier, il y a tous les événements futurs et optionnel.
Ce n’est pas obligatoire d’y assister.
Ils sont présents dans le calendrier pour nous donner la possibilité d’y assister si on veut.
Si il y a trop sur son calendrier, il suffit de cacher ce calendrier.

En parlant de LoL, ce n’est just pas facile de faire cela.

Il faut tout faire manuellement.

Il n’y a pas de moyens d’ajouter le programme de toute une ligue (ex: LCK ou LEC) ou une équipe au début de la saison.

Je pense que c’est une opportunité.

Ça ne devrait pas prendre trop de temps à un employer de ces ligues ou équipes de rendre publique sur par exemple Google Calendar ou par CalDav leurs matchs.

Présent dans le calendrier de beaucoup de personnes, il y a plus de chance qu’ils se connectent pour regarder.

* la concurrence est rude
La concurrence de la ligue est les autres ligues, les autres jeux, les sports.
Les autres médias qui consomment du temps sont aussi des concurrents (livres, podcasts, films, séries…)

Regarder un match de foot est plus commun que d’aller regarder un match de LoL.

Les gens iront sur le chemin de la moindre résistance.
Si regarder un match de foot est plus simple, il regardera un match de foot.
* ce que je fais
La majorité du temps, je rate le directe.

C’est parce qu’ils ne sont pas dans mon calendrier, du coup j’oublie.

Je n’ai pas besoin de savoir qui sont en finale.
Je sais qu’il y aura une cérémonie d’ouverture.
Généralement, c’est fun à regarder.
Pas besoin de savoir quelles équipes vont jouer.
C’est un plus.
Mais on sait déjà que ce sera T1 et GenG ou G2 et une autre équipe.

Ce qui fait qu’il n’y a pas vraiment besoin de mettre à jour le calendrier.
Le publier une fois en début du split suffit.

Je regarde alors les VODs.
Je les regarde en vitesse ×1.5.

Mais ce n’est pas vraiment la même chose.
Il m’arrive de déjà connaitre le résultat.
Ce n’est plus du direct, la tension est moins intense.

Ça reste fun.
Mais voir l’équipe gagner ou perdre plus de cinq heures plus tard fait qu’on perd un peu de la magie.
