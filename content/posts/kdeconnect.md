---
title: "KDE Connect"
date: 2022-12-29T10:11:08+03:00
draft: false
tags:
  - linux
  - opensource
  - software
---

KDE Connect https://kdeconnect.kde.org/ est une application qui permet la communication entre plusieurs appareils (ordinateurs, phones) connectés sur le même réseau.

Grâce au logiciel on peut par exemple: partager des fichiers; copier du texte de son téléphone pour le coller sur son ordinateur; recevoir les notifications d’un appareil sur un autre et d’intéragir avec (répondre aux messages…); contrôler le volume, jouer ou mettre en pause de la musique, lancer des commandes à distance.

Pour les utilisateurs de Gnome, il y a l’implémentation par GSconnect.
C’est ce que j’utilise.
Sur Android, l’application est KDE Connect et disponible sur F-Droid ou autres store.

Par la suite, par but de simplication et d’habitude je vais continue d’appeler les logiciels par KDE Connect.

# Exemples
## contrôler son ordinateur à distance avec son smartphone
On regarde une longue vidéo sur on ordinateur (oui un film, une série ou mieux un bon documentaire). 
Mais on n’est pas assit sur sa chaise de bureau. 
À la place on est dans son canapé.

Le volume de la scène est trop bas ou trop fort. 
La méthode classique est d’utiliser sa souris sans fil pour contrôler l’appareil à distance.

Avec KDE Connect, il suffit d’un clic pour controler le volume.
C’est quasiment une télécommande pour son ordinateur.

## contrôler son ordinateur à distance avec un autre ordinateur
On peut connecter deux ordinateurs (ou plus) avec KDE Connect. 
Ce n’est pas limité à une connection entre un ordinateur et son smartphone comme le montre [Nicco](https://www.youtube.com/watch?v=N8Uwh0hAhW8).


On peut facilement contrôler les lecteurs multimédias. 
Mettre en pause une musique, une vidéo, passer au suivant. 
Le classique est strict minimum.

Pour controler le volume c’est un peu plus compliqué. 
Utiliser les raccourcis clavier ou le bouton dédié sur son ordinateur principal intéragit avec cet ordinateur et non celui à distance.

La solution que j’utilise actuellement est d’utiliser des commandes.

Sur l’ordinateur à distance, utilisant Gnome, j’ai ajouté les commandes:

pour baisser le volume
```
pactl set-sink-volume @DEFAULT_SINK@ -10%
```
pour augmenter le volume
```
pactl set-sink-volume @DEFAULT_SINK@ +10%
```
pour mettre en sourdine et réactiver le son
```
pactl set-sink-mute @DEFAULT_SINK@ toggle
```

Ce sont les commandes de la communauté disponibles avec d’autres exemples sur https://userbase.kde.org/KDE_Connect/Tutorials/Useful_commands#Volume_control

Il faut ajouter les commandes sur l’ordinateur à distance. 
On n’envoie pas le commandes, on exécute celles présentes sur l’ordinateur.
## limites
###  être sur le même réseau
Si un appareil est connecté à un réseau dans un réseau alors il n’est pas possible de le connecté aux autres appareils.

Mon téléphone est connecté directement au routeur wifi de mon fournisseur d’accès à internet.
Mes ordinateurs sont connectés par un câble éthernet à un routeur qui lui est branché au routeur du fournisseur d’accès internet. 
Mon téléphone n’arrive pas à trouver mes ordinateurs, de même dans l’autre sens.

La solution serait de les connecter via un VPN dans mon réseau local, mais j’utilise déjà un VPN en permance sur mon téléphone, ce qui rend les choses difficiles.

### changer le volume en utilisant des lignes de commandes
La limite est qu’on n’a pas de retour visuel sur l’action qu’on vient d’effectuer: pas de notification visuelle.

On peut spécifier directement le volume qu’on désire (20%, 50%, 100%…) ce qui fait qu’on n’a pas besoin vraiment de retour visuel. 
On sait qu’en activant la commande, le volume sera toujours à 50%. 
Si la commande a bien fonctionner, le volume change. 
Pourtant, si on monte de 5%, la différence n’est pas forcément directement perçue.

Ici la commande pour ajuster le volume à exactement 42%:
```
pactl set-sink-volume @DEFAULT_SINK@ 42%
```

# ne pas oublier de configurer son firewall
J’utilise Uncomplicated Firewall aka UFW
```
sudo ufw allow 1714:1764/udp
sudo ufw allow 1714:1764/tcp
sudo ufw reload
```

# Pour plus de documentation
https://userbase.kde.org/KDEConnect
